# Dev basics 

This project was made as a technical test.
It's a web app using Javascript & Bootstrap.
It shows my favourite places.

## Initital Setup
After cloning navigate to the folder in your terminal.

Run `npm install` to install all dependencies.

## Running project

Run `npm run build:dev` to run a  dev server.

Navigate to `http://localhost:8080/`. The app will automatically reload if you change any of the source files.
