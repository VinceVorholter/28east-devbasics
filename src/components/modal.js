export class Modal {

    //Properties of class
    // modalElement = stores modal element
    // backdropElement = stores backdrop element

    constructor(contentId, fallbackMessage) {
        this.fallbackMessage = fallbackMessage;

        this.contentTemplateEl = document.getElementById(contentId);
        this.modalTemplateEl = document.getElementById('modal-template');

    }

    show(){
        //Check for compatibility
        if('content' in document.createElement('template')){
            // get all elements of the modal
            const modalElements = document.importNode(this.modalTemplateEl.content, true);
            // get individual elements
            this.modalElement = modalElements.querySelector('.modal'); 
            this.backdropElement = modalElements.querySelector('.backdrop'); 

            // 
            const contentElement = document.importNode(this.contentTemplateEl.content, true);

            this.modalElement.appendChild(contentElement);

            document.body.insertAdjacentElement('afterbegin', this.modalElement);
            document.body.insertAdjacentElement('afterbegin', this.backdropElement);

        } else {
            alert(this.fallbackMessage);
        }
    }

    hide(){
        if(this.modalElement){
            document.body.removeChild(this.modalElement);
            document.body.removeChild(this.backdropElement);
            // Just to remove any references to the DOM elements
            this.modalElement = null; 
            this.backdropElement = null; 
        }
    }
}