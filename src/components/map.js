export class Map {
    //Map Properties

    constructor(coordinates) {
        this.render(coordinates);
    }

    render(coordinates) {
        // test if global google variable exists
        if(!google){
            alert('Could not load maps. Please retry');
            return;
        }

        const mapElement = document.getElementById('map_canvas');

        const map = new google.maps.Map(mapElement, {
            center: coordinates,
            zoom: 18
        });

        new google.maps.Marker({
            position: coordinates,
            map: map
        })

    }

}