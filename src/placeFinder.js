import {
    Modal
} from './components/modal';
import {
    Map
} from './components/map';
import {
    getCoords
} from './util/location';

class PlaceFinder {
    // PlaceFinder Properties
    // map - stares a google map instance

    constructor() {

        const allFilter = document.getElementById('allFilter');
        const coffeeFilter = document.getElementById('coffeeFilter');
        const entFilter = document.getElementById('entFilter');
        const restFilter = document.getElementById('restFilter');
        const cardLinks = document.getElementsByClassName('card-link');

        allFilter.addEventListener('click', this.filter.bind(this));
        coffeeFilter.addEventListener('click', this.filter.bind(this));
        entFilter.addEventListener('click', this.filter.bind(this));
        restFilter.addEventListener('click', this.filter.bind(this));
        
        for(var i = 0; i < cardLinks.length; i++){
            const mapPos = cardLinks[i].dataset.mapLoc;
            //Wraping function in a function stops it calling itself
            cardLinks[i].addEventListener('click', ()=>{this.locatePlace(mapPos)});
        };

    }

    selectPlace(coordinates) {
        // Check if map obejct exists to prevent making new map instance
        if (this.map) {
            this.map.render(coordinates);
        } else {
            this.map = new Map(coordinates);
        }
    }

    locateUserHandler() {
        if (!navigator.geolocation) {
            alert('Location feature not available. Please use another browser');
            return;
        }

        const modal = new Modal('loading-modal-content', 'Fetching location. Please wait');
        modal.show();

        navigator.geolocation.getCurrentPosition(
            successResult => {
                const coordinates = {
                    lat: successResult.coords.latitude,
                    lng: successResult.coords.longitude
                };
                this.selectPlace(coordinates);
                modal.hide();
            }, error => {
                modal.hide();
                alert('Could find your location. Please enter an address or try again')
            }
        );
    }

    async findAddressHandler(event) {
        event.preventDefault();

        const address = event.target.querySelector('input').value;

        if (!address || address.trim().length === 0) {
            alert("Invalid Address");
            return;
        }

        const modal = new Modal('loading-modal-content', 'Fetching location. Please wait');

        modal.show();
        try {
            const coordinates = await getCoords(address);
            this.selectPlace(coordinates);
            modal.hide();
        } catch (err) {
            alert(err.message);
        }
    }

    async locatePlace(placeAddress) {
        
        console.log(placeAddress);

        if (!placeAddress || placeAddress.trim().length === 0) {
            alert("Invalid Address");
            return;
        }

        try {
            const coordinates = await getCoords(placeAddress);
            console.log(coordinates)
            this.selectPlace(coordinates);

        } catch (err) {
            alert(err.message);
        }


    }

    filter(event) {
        event.preventDefault();
        const filter = event.target.innerHTML.toLowerCase();
        this.toggleActive(filter);
        const cardArray = document.getElementsByClassName('card');

        // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
        cardArray.forEach(card => {
            if (filter === 'all') {
                card.classList.add('show');
                card.classList.remove('hide');
            } else if (card.classList.contains(filter)) {
                card.classList.add('show');
                card.classList.remove('hide');
            } else {
                card.classList.add('hide');
                card.classList.remove('show');
            }
        });
    }

    toggleActive(selected) {
        const pillArray = document.getElementsByClassName('nav-link');
       
        pillArray.forEach(pill => {
            if (pill.innerHTML.toLowerCase() == selected) {
                pill.classList.add('active');
            } else {
                pill.classList.remove('active');
            }
        });
    }

}

const placeFinder = new PlaceFinder();
